<?php

namespace App\Aggregator;

class SolidSoftAggregator
{
    private $acces_token;
    private $env;
    private $url_epeater_left_sperator = '<';
    private $url_epeater_right_sperator = '>';
    private $bulk_pack_param_schema = array('productCodeScheme','productCode','serialNumber','batchId','expiryDate');
    
    public $httpRequestStatus = [
        ["202","Post Bulk-of-Pack","No information provided","","00","","22100000",""  ],
        ["409","Post Bulk-of-Pack","Warning","","00","Yes","52120000","Duplicate serial numbers provided.  An alert has been raised."  ],
        ["422","Post Bulk-of-Pack","Warning","","01","","62120001","The actual number of packs does not match the stated number."  ],
        ["422","Post Bulk-of-Pack","Warning","","02","","62120002","The request body is invalid."  ],
        ["422","Post Bulk-of-Pack","Warning","","03","","62120003","Too many packs in bulk request.  A maximum of {0} packs will be accepted."  ],
        ["422","Post Bulk-of-Pack","Warning","","04","","62120004","The emvs-data-entry-mode header is required."  ],
        ["422","Post Bulk-of-Pack","Warning","","05","","62120005","The emvs-data-entry-mode header is invalid."  ],
        ["422","Post Bulk-of-Pack","Warning","","06","","62120006","Manually entered bulk of pack requests are not supported."  ],
        ["422","Post Bulk-of-Pack","Warning","","07","","62120007","The requested astte is invalid."  ],
        ["200","Get Bulk-of-Pack Results","No information provided","","00","","12200000",""  ],
        ["404","Get Bulk-of-Pack Results","Warning","","00","","42220000","No results found.  The results may have expired."  ],
        ["409","Get Bulk-of-Pack Results","Information","","00","","52210000","The request is still being processed."  ],
        ["403","Common","Warning","","00","","30020000","The client system is not authorised to perform this request."  ],
        ["429","Common","Warning","","00","","70020000","The National System is handling an unusually high volume of requests. Please resend the request later."  ],
        ["404","Single Pack","No information provided","","00","","41000000",""  ],
        ["404","Single Pack","Warning","","00","","41020000","The product code is unknown."  ],
        ["404","Single Pack","Warning","","01","Yes","41020001","The serial number is unknown.  An alert has been raised."  ],
        ["404","Single Pack","Warning","","02","","41020002","The serial number is unknown."  ],
        ["404","Single Pack","Warning","","03","Yes","41020003","The batch identifier mismatches the recorded batch identifier.  An alert has been raised."  ],
        ["404","Single Pack","Warning","","04","","41020004","The batch identifier mismatches the recorded batch identifier."  ],
        ["404","Single Pack","Warning","","05","Yes","41020005","The expiry date mismatches the recorded expiry date.  An alert has been raised."  ],
        ["404","Single Pack","Warning","","06","","41020006","The expiry date mismatches the recorded expiry date."  ],
        ["404","Single Pack","Warning","","00","Yes","41020007","Too many incorrect manual data entry attempts.  An alert has been raised."  ],
        ["422","Single Pack","Warning","","00","","61020000","A batch identifier is required."  ],
        ["422","Single Pack","Warning","","01","","61020001","A product code is required."  ],
        ["422","Single Pack","Warning","","02","","61020002","A product code scheme is required."  ],
        ["422","Single Pack","Warning","","03","","61020003","A serial number is required."  ],
        ["422","Single Pack","Warning","","04","","61020004","An expiry date is required."  ],
        ["422","Single Pack","Warning","","05","","61020005","The {0} product code scheme is not supported."  ],
        ["422","Single Pack","Warning","","06","","61020006","The batch identifier is invalid."  ],
        ["422","Single Pack","Warning","","07","","61020007","The expiry date is invalid."  ],
        ["422","Single Pack","Warning","","08","","61020008","The product code is invalid."  ],
        ["422","Single Pack","Warning","","09","","61020009","A requested state is required."  ],
        ["422","Single Pack","Warning","","10","","61020010","The requested state is invalid."  ],
        ["422","Single Pack","Warning","","11","","61020011","The serial number is invalid."  ],
        ["422","Single Pack","Warning","","12","","61020012","The emvs-data-entry-mode header is required."  ],
        ["422","Single Pack","Warning","","13","","61020013","The emvs-data-entry-mode header is invalid."  ],
        ["422","Single Pack","Warning","","14","","61020014","The request body is invalid."  ],
        ["200","Decommission Single Pack","Information","","00","","11310600","The pack has been marked as a free sample."  ],
        ["200","Decommission Single Pack","Information","Sample","00","","11310500","The pack has been marked as a sample."  ],
        ["200","Decommission Single Pack","Information","Destroyed","00","","11310400","The pack has been marked as destroyed."  ],
        ["200","Decommission Single Pack","Information","Exported","00","","11310800","The pack has been marked as exported from the EU."  ],
        ["200","Decommission Single Pack","Information","Locked","00","","11310700","The pack has been marked as locked."  ],
        ["200","Decommission Single Pack","Information","Stolen","00","","11310300","The pack has been marked as stolen."  ],
        ["409","Decommission Single Pack","Warning","Supplied","00","Yes","51320200","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Exported","00","Yes","51320800","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Stolen","00","Yes","51320300","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Destroyed","00","Yes","51320400","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Sample","00","Yes","51320500","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Free Sample","00","Yes","51320600","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Locked","00","Yes","51320700","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Checked-Out","00","Yes","51320900","The pack cannot be decommissioned.  An alert has been raised."  ],
        ["409","Decommission Single Pack","Warning","Expired","00","Yes","51321000","The pack cannot be decommissioned.  The batch has expired."  ],
        ["409","Decommission Single Pack","Warning","Recalled","00","","51321100","The pack cannot be decommissioned.  The batch has been recalled."  ],
        ["409","Decommission Single Pack","Warning","Withdrawn","00","","51321200","The pack cannot be decommissioned.  The product has been withdrawn."  ],
        ["200","Reactivate Single Pack","Information","Active","00","","11410100","The pack has been reactivated."  ],
        ["409","Reactivate Single Pack","Warning","Stolen","00","","51420300","The pack cannot be reactivated."  ],
        ["409","Reactivate Single Pack","Warning","Destroyed","00","","51420400","The pack cannot be reactivated."  ],
        ["409","Reactivate Single Pack","Warning","Checked-Out","00","Yes","51420900","The pack cannot be reactivated.  An alert has been raised."  ],
        ["200","Reactivate Single Pack","Warning","Active","00","","11420100","The pack cannot be reactivated.  It is already active."  ],
        ["409","Reactivate Single Pack","Warning","Sample","00","","51420500","The pack cannot be reactivated.  It was decommissioned at another location."  ],
        ["409","Reactivate Single Pack","Warning","Sample","01","","51420501","The pack cannot be reactivated.  Time limit exceeded."  ],
        ["409","Reactivate Single Pack","Warning","Free Sample","00","","51420600","The pack cannot be reactivated.  It was decommissioned at another location."  ],
        ["409","Reactivate Single Pack","Warning","Free Sample","01","","51420601","The pack cannot be reactivated.  Time limit exceeded."  ],
        ["409","Reactivate Single Pack","Warning","Locked","00","","51420700","The pack cannot be reactivated.  It was decommissioned at another location."  ],
        ["409","Reactivate Single Pack","Warning","Exported","00","","51420800","The pack cannot be reactivated.  It was decommissioned at another location."  ],
        ["409","Reactivate Single Pack","Warning","Exported","01","","51420801","The pack cannot be reactivated.  Time limit exceeded."  ],
        ["409","Reactivate Single Pack","Warning","Recalled","00","","51421100","The pack cannot be reactivated.  The batch has been recalled."  ],
        ["200","Reactivate Single Pack","Warning","Recalled","00","","11421100","The batch has been recalled in this market. The pack will be reactivated in any markets where the batch is not recalled."  ],
        ["409","Reactivate Single Pack","Warning","Expired","00","","51421000","The pack cannot be reactivated.  The batch has expired."  ],
        ["409","Reactivate Single Pack","Warning","Withdrawn","00","","51421200","The pack cannot be reactivated.  The product has been withdrawn."  ],
        ["200","Reactivate Single Pack","Warning","Withdrawn","00","","11421200","The product has been withdrawn in this market. The pack will be reactivated in any markets where the product is not withdrawn."  ],
        ["409","Reactivate Single Pack","Warning","Supplied","00","","51420200","The pack cannot be reactivated.  Time limit exceeded."  ],
        ["409","Reactivate Single Pack","Warning","Supplied","01","","51420201","The pack cannot be reactivated.  It was dispensed at another location."  ],
        ["200","Supply Single Pack","Information","Supplied","00","","11210200","The pack has been dispensed."  ],
        ["200","Supply Single Pack","Warning","Supplied","00","","11220200","The pack was previously dispensed at this location."  ],
        ["200","Supply Single Pack","Warning","Supplied","01","","11220201","The pack was previously dispensed at this location.  The next attempt will be rejected."  ],
        ["409","Supply Single Pack","Warning","Supplied","00","Yes","51220200","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Supplied","01","Yes","51220201","The pack was previously supplied at this location. Too many repeated attempts. An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Exported","00","Yes","51220800","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Stolen","00","Yes","51220300","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Destroyed","00","Yes","51220400","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Sample","00","Yes","51220500","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Free Sample","00","Yes","51220600","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Locked","00","Yes","51220700","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Checked-Out","00","Yes","51220900","The pack cannot be dispensed.  An alert has been raised."  ],
        ["409","Supply Single Pack","Warning","Recalled","00","","51221100","The pack cannot be dispensed.  The batch has been recalled."  ],
        ["409","Supply Single Pack","Warning","Expired","00","","51221000","The pack cannot be dispensed.  The batch has expired."  ],
        ["409","Supply Single Pack","Warning","Withdrawn","00","","51221200","The pack cannot be dispensed.  The product has been withdrawn."  ],
        ["200","Verify Single Pack","Information","Active","00","","11110100","The pack is active."  ],
        ["200","Verify Single Pack","Information","Recalled","00","","11111100","The batch has been recalled."  ],
        ["200","Verify Single Pack","Information","Expired","00","","11111000","The batch has expired."  ],
        ["200","Verify Single Pack","Information","Supplied","00","","11110200","The pack has been dispensed."  ],
        ["200","Verify Single Pack","Information","Checked-Out","00","","11110900","The pack has been re-packed."  ],
        ["200","Verify Single Pack","Information","Free Sample","00","","11110600","The pack is marked as a free sample."  ],
        ["200","Verify Single Pack","Information","Sample","00","","11110500","The pack is marked as a sample."  ],
        ["200","Verify Single Pack","Information","Destroyed","00","","11110400","The pack is marked as destroyed."  ],
        ["200","Verify Single Pack","Information","Exported","00","","11110800","The pack is marked as exported from the EU."  ],
        ["200","Verify Single Pack","Information","Locked","00","","11110700","The pack is marked as locked."  ],
        ["200","Verify Single Pack","Information","Stolen","00","","11110300","The pack is marked as stolen."  ],
        ["200","Verify Single Pack","Information","Withdrawn","00","","11111200","The product has been withdrawn."  ],
        ["403","Verify Single Pack","Warning","","00","","31120000","The pack cannot be verified.  Too many verification attempts."  ]
    ];
    
    const SCHEMA_TYPE_GSONE = 1;
    const SCHEMA_TYPE_GTIN = 2;
    const SCHEMA_TYPE_NTIN = 3;
    const SCHEMA_TYPE_IFA = 4;
    const SCHEMA_TYPE_PPN = 5;

    static public $schemaTypeString = array(
        self::SCHEMA_TYPE_GSONE => 'gs1',
        self::SCHEMA_TYPE_GTIN => 'gtin',
        self::SCHEMA_TYPE_NTIN => 'ntin',
        self::SCHEMA_TYPE_IFA => 'ifa',
        self::SCHEMA_TYPE_PPN => 'ppn',
    );
    
    public function httpRequestStatusOrderByOperationCode()
    {
        $result = array();
        foreach($this->httpRequestStatus as $status){
            $result[$status[6]] = array(
                'http_code' => $status[0],
                'api_operation' => $status[1],
                'info_type' => $status[2],
                'pack_state' => $status[3],
                'reason' => $status[4],
                'alert' => $status[5],
                'text' => $status[7],
            );
        }
        $this->http_status_operation_code = $result;
    }
    
    public function getHttpRequestStatus($operationCode)
    {
        return (isset($this->http_status_operation_code[$operationCode]))? $this->http_status_operation_code[$operationCode] : false;
    }
    
    static function getSchemaStrign($type)
    {
        return self::$schemaTypeString[$type];
    }
    
    const URL_TYPE_SUPPLY_SINGLE = 1;
    const URL_TYPE_BULK_PACK = 2;
    
    static public $urlTypeString = array(
        self::URL_TYPE_SUPPLY_SINGLE => 'http://<base_url>/product/<scheme>/<productCode>/pack/<serialNumber>?batch=<batchId>&expiry=<expiry>',
        self::URL_TYPE_BULK_PACK => 'http://<base_url>/product/packs'
    );
    
    static function getUrlStrign($type)
    {
        return self::$urlTypeString[$type];
    }
    
    public function __construct()
    {
        $envVariables = config('solidsoft.env.startup');
        $this->env = $envVariables;
        $this->httpRequestStatusOrderByOperationCode();
        $auth = $this->auth();
    }
    
    public function getHeader()
    {
        return array(
            'Content-Type: application/json',
            sprintf('Authorization: Bearer %s', $this->acces_token),
            'emvs-data-entry-mode: ' .$this->env['data_entry_mode'],
            $this->env['accept_language'].' : ' .$this->env['language']
        );
    }
    
    public function auth()
    {
        $client = new \App\Aggregator\CurlAggregator();
        $headers = null;
        $body = array('client_id' => $this->env['client_id'],
                      'client_secret' => $this->env['client_secret'],
                      'grant_type'  => 'client_credentials');
        $client->post($this->env['access_token_url'], $headers, $body);
        $response = $client->getBody();
        $arrayData = json_decode($response, true);
        if(isset($arrayData['access_token']) && !empty($arrayData['access_token'])){
           $this->acces_token = $arrayData['access_token'];
        }else{
           return  false;
        }
    }
    
    public function supplySingle($productCode, $serialNumber, $batchId, $expiry)
    {
        $url_schema_params = array(
            'base_url' => $this->env['base_url'],
            'scheme' => $this->getSchemaStrign(self::SCHEMA_TYPE_GTIN),
            'productCode' => $productCode,
            'serialNumber' => $serialNumber,
            'batchId' => $batchId,
            'expiry' => $expiry
        );
        $url = $this->urlRepeater($this->getUrlStrign(self::URL_TYPE_SUPPLY_SINGLE), $url_schema_params);
        $client = new \App\Aggregator\CurlAggregator();
        $headers = $this->getHeader();
        $client->get($url, $headers);
        $response = $client->getBody();
        $this->result = json_decode($response, true);
        return $this;
    }
    
    public function bulkPack($products)
    {
        $url = $this->urlRepeater($this->getUrlStrign(self::URL_TYPE_BULK_PACK), array('base_url' => $this->env['base_url']));
        $headers = $this->getHeader();
        $client = new \App\Aggregator\CurlAggregator();
        $body = $this->bulkPackBody($products);
        $client->post($url, $headers, $body, true);
        $response = $client->getBody();
        $this->result = json_decode($response, true);
        return $this;
    }
    
    public function bulkPackBody($products)
    {
        $body = array();
        $result = array();
        $coutnProduct = count($products);
        foreach($products as $product)
        {
            $setDefProductSchema = (count($product) == count($this->bulk_pack_param_schema))? true : false;
            $defProductSchema = $this->getHttpRequestStatus(self::SCHEMA_TYPE_GSONE);
            $result[] = array(
                $this->bulk_pack_param_schema[0] => ($setDefProductSchema)? $product[0] : $defProductSchema,
                $this->bulk_pack_param_schema[1] => ($setDefProductSchema)? $product[1] : $product[0],
                $this->bulk_pack_param_schema[2] => ($setDefProductSchema)? $product[2] : $product[1],
                $this->bulk_pack_param_schema[3] => ($setDefProductSchema)? $product[3] : $product[2],
                $this->bulk_pack_param_schema[4] => ($setDefProductSchema)? $product[4] : $product[3],
            );
        }
        $body = array(
            'numberOfPacks' => $coutnProduct,
            'packs' => $result
        );
        return json_encode($body);
    }
    
    public function urlRepeater($url_schema, $params = array())
    {
        $result = $url_schema;
        foreach($params as $key=>$param){
            $result = str_replace($this->url_epeater_left_sperator.$key.$this->url_epeater_right_sperator, $param, $result);
        }
        return $result;
    }
    
    public function appendHttpStatus()
    {
        $reuslt = $this->getHttpRequestStatus($this->result['operationCode']);
        $this->result += $reuslt;
        return $this;
    }
    
    public function createBulk()
    {
        unset($this->result);
        $this->bulk_products = array();
        return $this;
    }
    
    public function appendProduct($productCode, $serialNumber, $batchId, $expiryDate, $productCodeScheme = self::SCHEMA_TYPE_GSONE)
    {
        $productCodeSchemeString = $this->getSchemaStrign($productCodeScheme);
        $result = array(
            $this->bulk_pack_param_schema[0] => $productCodeSchemeString,
            $this->bulk_pack_param_schema[1] => $productCode,
            $this->bulk_pack_param_schema[2] => $serialNumber,
            $this->bulk_pack_param_schema[3] => $batchId,
            $this->bulk_pack_param_schema[4] => $expiryDate,
        );
        $this->bucl_products[] = $result;
        return $this;
    }

    public function send()
    {
        if(!empty($this->bucl_products)){
            $url = $this->urlRepeater($this->getUrlStrign(self::URL_TYPE_BULK_PACK), array('base_url' => $this->env['base_url']));
            $counterProducts = count($this->bucl_products);
            $body = json_encode(array(
                'numberOfPacks' => $counterProducts,
                'packs' => $this->bucl_products
            ));
            $headers = $this->getHeader();
            $client = new \App\Aggregator\CurlAggregator();
            $client->post($url, $headers, $body, true);
            $response = $client->getBody();
            $this->result = json_decode($response, true);
            return $this;
        }else{
            return false;
        }
    }
}