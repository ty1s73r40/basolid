<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Aggregator\SolidSoftAggregator;
class SolidSoftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = array(
                array ('gs1','15000436574634','25005553064','7654321','201201'),
                array ('gs1','20000605901171','4883658350','RP573V30','210101') 
            );
        
        $solidsoft = new SolidSoftAggregator();
        
        $result['supply'] = $solidsoft->supplySingle('03734679285686', '0000000004', '00001', '201201')
                            ->appendHttpStatus()
                            ->result;
        
        $result['bulk'] = $solidsoft->bulkPack($products)
                            ->appendHttpStatus()
                            ->result;
        
        $result['bulk_second'] = $solidsoft->createBulck()
                ->appendProduct('15000436574634', '25005553064', '7654321', '201201')
                ->appendProduct('20000605901171', '4883658350', 'RP573V30', '210101')
                ->appendHttpStatus()
                ->send()
                ->result;
        
        var_dump($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
