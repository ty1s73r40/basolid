<?php

namespace App\Aggregator;

class CurlAggregator
{
    public $responce;
    
    const TYPE_REQUEST_POST = 1;
    const TYPE_REQUEST_GET = 2;
    const TYPE_REQUEST_PUT = 3;
    const TYPE_REQUEST_DELETE = 4;
    
    public function get($url, $headers = null)
    {
        $this->execute_curl($url, $headers);
    }
    
    public function execute_curl($url, $headers, $type = self::TYPE_REQUEST_GET, $fieldString = '')
    {
        if($type != self::TYPE_REQUEST_GET){
            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
        }else{
            $ch = curl_init($url);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(!is_null($headers)){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        if($type != self::TYPE_REQUEST_GET){
            $fieldFromString = explode("&", $fieldString);
            curl_setopt($ch,CURLOPT_POST, count($fieldFromString)-1);
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fieldString);
            curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        }
        $this->responce = curl_exec($ch);
        curl_close($ch);
    }
    
    public function getBody()
    {
        return $this->responce;
    }
    
    public function post($url, $headers, $fields = array(), $directFields = false)
    {
        $makePostFieldsString = ($directFields)? $fields : $this->postFieldCreator($fields);
        $this->execute_curl($url, $headers, self::TYPE_REQUEST_POST, $makePostFieldsString);
    }
    
    public function postFieldCreator($fields)
    {
        $fields_string = '';
        foreach($fields as $key=>$value)
        {
            $fields_string .= $key.'='.$value.'&';
        }
        rtrim($fields_string, '&');
        return $fields_string;
    }
    
}
